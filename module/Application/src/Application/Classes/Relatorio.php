<?php

namespace Application\Classes;

/**
 * A classe Relatorio tem objetivo facilitar a criação de relatorios
 * e deixá-los em um formato padrão e para fcilitar a manutenção. 
 * @author Robson Martins Lopes
 * @since 27/01/2016
 * @email robmlopes@yahoo.com.br
 * @skype robmlopes_2
 */
/*
 * Este programa é um software livre; você pode redistribuí-lo e/ou 
 * modificá-lo dentro dos termos da Licença Pública Geral GNU como 
 * publicada pela Fundação do Software Livre (FSF); na versão 3 da 
 * Licença, ou (na sua opinião) qualquer versão.
 * Este programa é distribuído na esperança de que possa ser útil, 
 * mas SEM NENHUMA GARANTIA; sem uma garantia implícita de ADEQUAÇÃO
 * a qualquer MERCADO ou APLICAÇÃO EM PARTICULAR. Veja a
 * Licença Pública Geral GNU para maiores detalhes.
 */

class Relatorio {

    private $desc_campo = array();
    private $db_campo = array();
    private $tipo_campo = array();
    private $tamanho_coluna = array();
    private $alinhamento_coluna = array();
    private $coluna;
    private $alterna_cor;
    private $cor_fundo;
    private $quebra_campo;
    private $quebra_descricao;
    private $quebra_anterior;
    private $quebra_cabecalho;
    private $novo_cabecalho;
    private $sub_total_exibir;
    private $zerar_sub_total;
    private $link;

    public function __construct() {
        $this->coluna = 0;
        $this->alterna_cor = array(true, '#F5F6CE', '#CEF6CE');
        $this->sub_total_exibir = 0;
        $this->zerar_sub_total = 0;
    }
    /**
     * Método para definir link para uma coluna
     * @param string $coluna Para qual coluna o link está sendo definido [$db_campo] de definirColuna()
     * @param string $link O link [href] para onde será redirecionado (obs: pode ser uma função javascript)
     * @param integer $target [opcional valor default: 0]<p>
     * 0: para abrir o link na mesma página</p><p>
     * 1: para abrir o link em nova guia</p>
     */
    public function definirLink($coluna, $link, $target=0) {
        $this->link[$coluna] = array($coluna, $link, $target);        
    }
    /**
     * Método para definir as configurações de cada coluna a ser exibida no relatório 
     * @param string $descricao Descrição da exibição da coluna 
     * @param string $db_campo <p>
     *  Nome do campo que vem do resultado do banco de dados ou
     *  nome de variavel a ser utilizada </p>
     * @param string $tam_coluna Tamanho da coluna para exibição
     * @param string $alinhamento Tipo de alinhamento do conteúdo
     * @param string $tipo_coluna Tipo de conteúdo que será exibido ['t'] ou ['0-9'] para quantidade de casas decimais
     * @param string $calc_total Se a coluna será calculado a somatória para exibir total
     * 
     **/
    public function definirColuna($descricao, $db_campo, $tam_coluna, $alinhamento, $tipo_coluna, $calc_total) {
        $this->desc_campo[$db_campo] = $descricao;
        $this->db_campo[$db_campo] = $db_campo;
        $this->tamanho_coluna[$db_campo] = $tam_coluna;
        $this->alinhamento_coluna[$db_campo] = $this->setAlinhamento($alinhamento);
        $this->tipo_campo[$db_campo] = $tipo_coluna;
        $this->calc_total[$db_campo] = $calc_total;
        if ($calc_total === "s") {
            $this->total[$db_campo] = array(0, array($this->setAlinhamento($alinhamento), $tipo_coluna));
            $this->sub_total[$db_campo] = array(0, array($this->setAlinhamento($alinhamento), $tipo_coluna));
        } else {
            $this->total[$db_campo] = array("", array($this->setAlinhamento($alinhamento), $tipo_coluna));
            $this->sub_total[$db_campo] = array("", array($this->setAlinhamento($alinhamento), $tipo_coluna));
        }
        $this->coluna = $this->coluna + 1;
    }
    /**
     * Método para definir as configurações de cada quebra a ser exibida no relatório 
     * @param string $descricao Descrição da exibição da coluna 
     * @param string $db_campo <p>
     *  Nome do campo que vem do resultado do banco de dados ou
     *  nome de variavel a ser utilizada para realizar a quebra</p>
     * @param string $quebra_anterior O nome do campo da quebra anterior caso tenha uma quebra anterior definida,
     * @param string $cabecalho Nome do campo que vira do banco de dados para ser concatenado com a descrição da quebra
     * 
     **/
    public function definirQuebra($descricao, $db_campo, $quebra_anterior, $cabecalho) {
        $this->quebra_campo[$db_campo] = strtolower($db_campo);
        $this->quebra_descricao[$db_campo] = $descricao;
        $this->quebra_anterior[$db_campo] = strtolower($quebra_anterior);
        $this->quebra_cabecalho[$db_campo] = strtolower($cabecalho);
        if (strlen($cabecalho) > 0) {
            $this->novo_cabecalho[$db_campo] = strtolower($cabecalho);
        }
        $this->quebra_anterior[$db_campo] = "";
        //$this->link[$db_campo] = array($db_campo,'',0);
    }
    /**
     * Gera o cabeçalho do relatório criando a tabela com a descrição de cada coluna definida em definirColuna()
     * @param integer $tamanho Tamanho da tabela do relatório<p>
     * Pode ser de [1-12] que usa a classe de tamanho do bootstrap col-md-[1-12]</p>
     */
    public function gerarCabecalho($tamanho) {
        echo "<div class='col-md-" . $tamanho . "'>";
        echo "<table id='relatorio' class='tablesorter table table-bordered table-responsive table-hover'>";
        echo "<thead>";
        echo "<tr>";
        foreach ($this->desc_campo as $desc) {
            echo "<th class='text-center' bgcolor='#81BEF7'>" . $desc . "</th>";
        }
        echo "</tr>";
        echo "</thead>";
        echo "<tbody>";
    }
    /**
     * Método responsável por receber uma linha do resultSet por vez e montar o corpo do relatorio 
     * @param resultSet $linha Recebe uma linha por vez para montar o relatório
     */
    public function gerarRelatorio($linha) {        
        if ($this->alterna_cor[0] === true) {
            $this->cor_fundo = $this->alterna_cor[1];
            $this->alterna_cor[0] = false;
        } else {
            $this->cor_fundo = $this->alterna_cor[2];
            $this->alterna_cor[0] = true;
        }
        $this->exibirQuebra($linha);        
        echo "<tr>";        
        foreach ($this->db_campo as $campo) {
            $align = $this->alinhamento_coluna[$campo];
            //verifica se o valor do conteúdo é um numero para calcular o sub_total e total 
            if (is_numeric($this->tipo_campo[$campo]) === true) {
                $valor_imprimir = number_format($linha[$campo], $this->tipo_campo[$campo], ",", ".");                
                $this->calcularTotal($linha, $campo);                       
            } else {
                $valor_imprimir = $linha[$campo];
            }
            echo "<td width='" . $this->db_campo[$campo] . "' class='" . $align . "' bgcolor='" . $this->cor_fundo . "'>" ;
            //verifica se tem link e monta o  link com o valor para imprimir
            echo $this->imprimirValor($linha, $campo, $valor_imprimir);
            echo "</td>";
        }        
        echo "</tr>";
        if($this->zerar_sub_total===1){
            $this->zerar_sub_total=0;
        }
    }
    
    /**
     * Verifica se a coluna possui um link e o cria caso tenha. O resultado de retorno será apenas um echo
     * com o valor da coluna formatado e com link caso algum tenha sido definido.
     * @param string $linha A linha com os campos do resultSet
     * @param string $campo O nome do campo(da coluna) que está sempre impresso no tabela
     * @param string $valor_imprimir O valor já formatado que será impresso na tabela
     * 
     */
    private function imprimirValor($linha, $campo, $valor_imprimir) {
        $abre_link = "";
        $fecha_link = "";
        if (count($this->link) > 0) {
            if (isset($this->link[$campo][0]) === true) {
                $link_redirecionar = $this->link[$campo][1];
                $variaveis = explode("[", $link_redirecionar);
                foreach ($variaveis as $chave => $valor) {
                    $item = explode("]", $valor);
                    $variaveis[$chave] = $item[0];
                }
                if (count($variaveis) > 0) {
                    foreach ($variaveis as $chave1 => $valor1) {
                        if ($chave1 > 0) {
                            $link_redirecionar = str_replace("[" . $valor1 . "]", $linha[$valor1], $link_redirecionar);
                        }
                    }
                }
                if ($this->link[$campo][2] === 1) {
                    $target = "_blank";
                } else {
                    $target = "";
                }
                $abre_link = "<a href='" . $link_redirecionar . "' target='" . $target . "'>";
                $fecha_link = "</a>";
            } else {
                $abre_link = "";
                $fecha_link = "";
            }
        }
        echo $abre_link . $valor_imprimir . $fecha_link; 
    }
    /**
     * Gera o rodapé do relatorio exibindo o total geral caso tenha alguma coluna de somatória e 
     * fecha a tabela do relatório
     */
    public function gerarRodape() {
        if (count($this->total) > 0) {
            $this->exibirTotalGeral();
        }
        echo "</tbody>";
        echo "</table>";
        echo "</div>";
    }
    private function calcularTotal($linha, $campo) {
        if ($this->calc_total[$campo] === "s") {
            $this->total[$campo][0] = $this->total[$campo][0] + $linha[$campo];
            if($this->zerar_sub_total===1){
                $this->sub_total[$campo][0] = 0 + $linha[$campo];
            }else{
                $this->sub_total[$campo][0] = $this->sub_total[$campo][0] + $linha[$campo];                    
            }
        }
    }
    /**
     * Exibe o a última linha do relatorio com a somatória de cada coluna definida como somatoria 
     */
    private function exibirTotalGeral() {
        if (count($this->quebra_campo) > 0) {$this->exibirSubTotal();}
        $align = "text-right";
        $cor = "#81BEF7";
        echo "<tr>";
        echo "<td class='text-left' bgcolor='" . $cor . "'><b>TOTAL</b></td>";
        $primeira_coluna = 1;
        foreach ($this->total as $total) {
            if (strlen($total[0]) === 0) {
                if ($primeira_coluna === 0) {
                    echo "<td bgcolor='" . $cor . "'></td>";
                } else {
                    $primeira_coluna = 0;
                }
            } else {
                echo "<td class='" . $total[1][0] . "' bgcolor='" . $cor . "'><b>" . number_format($total[0], $total[1][1], ",", ".") . "</b></td>";
            }
        }
        echo "</tr>";
    }
    /**
     * Exibe o uma linha com a somatória dos valores de cada coluna definida como somatoria contidos dentro de uma quebra
     */
    private function exibirSubTotal() {
        $align = "text-right";
        $cor = "#81BEF7";
        echo "<tr>";
        echo "<td class='text-left' bgcolor='" . $cor . "'>SUB-TOTAL</td>";
        $primeira_coluna = 1;
//        echo "<pre>";
//        var_dump($this->sub_total);
//        echo "</pre>";
        foreach ($this->sub_total as $sub_total) {
            if (strlen($sub_total[0]) === 0) {
                if ($primeira_coluna === 0) {
                    echo "<td bgcolor='" . $cor . "'></td>";
                } else {
                    $primeira_coluna = 0;
                }
            } else {
                echo "<td class='" . $sub_total[1][0] . "' bgcolor='" . $cor . "'>" . number_format($sub_total[0], $sub_total[1][1], ",", ".") . "</td>";
            }
        }
        echo "</tr>";
    }
    
    /**
     * Exibe todas as quebras de páginas definidas por definirQuebra()
     * @param array $linha Uma linha do resultSet que vem do BD 
     */
    private function exibirQuebra($linha) {
        if (count($this->quebra_campo) > 0) {
            foreach ($this->quebra_campo as $quebra) {
//                se a quebra atual for diferente da quebra anterior exibir a quebra
                if ($linha[$quebra] !== $this->quebra_anterior[$quebra]) {
//                  codição para não exibir o sub-total como a primeira linha do relatorio
                    if (count($this->sub_total) > 0 && $this->sub_total_exibir===1) {
                        $this->exibirSubTotal();
                        $this->zerar_sub_total = 1;
                        $this->sub_total_exibir=0;
                    }
                    $desc_quebra = $this->quebra_descricao[$quebra] . $linha[$this->quebra_cabecalho[$quebra]];
                    $colspan = count($this->db_campo);
                    $cor = "#81BEF7";
                    
                    echo "<tr>";
                    echo "<td class='text-left' bgcolor='" . $cor . "' colspan='" . $colspan . "'><b>" . $desc_quebra . "</b></td>";
                    echo "</tr>";
                    $this->quebra_anterior[$quebra] = $linha[$quebra];
                }
            }
//            após gerar as primeiras quebras permite exibir o subtotal            
            $this->sub_total_exibir=1;
        }
    }
    /**
     * Método que recebe um valor string e traduz para uma classe de alinhamento do bootstrap
     * ou dispara uma exceção 
     * @param string $tipo Tipo de alinhamentro do texto da coluna<p>
     * Valores permitidos [center|left|right]</p>
     * @return string Retorna a classe do bootstrap referente ao alinhamento de texto
     * @throws \Exception Dispara uma exceção caso o valor do parâmetro seja invalido
     */
    private function setAlinhamento($tipo='') {
        switch ($tipo) {
            case 'center':
                return 'text-center';
                break;
            case 'left':
                return 'text-left';
                break;
            case 'right':
                return 'text-right';
                break;
            default:
                throw new \Exception("O parâmetro '$tipo' em setAlinhamento('$tipo') deve ser [center|left|right]");
                return "erro";
        }
    }

}
