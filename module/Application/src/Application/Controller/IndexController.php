<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2015 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Application\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;

//importação da classe de relatorio
use Application\Classes\Relatorio;

class IndexController extends AbstractActionController
{
    public function indexAction()
    {
        //instância do objeto relatorio
        $rel = new Relatorio();
        
        //definição das colunas do relatorio
        $rel->definirColuna('Código'            , 'codigo'      , '10', 'center', 't', 'n');
        $rel->definirColuna('Descrição'         , 'descricao'   , '50', 'left'  , 't', 'n');
        $rel->definirColuna('Valor Unit.(R$)'   , 'valor'       , '20', 'right' , '2', 's');
        $rel->definirColuna('Peso Unit.(Kg)'    , 'peso'        , '20', 'right' , '3', 's');
        
        //exemplo de definição de quebra
        $rel->definirQuebra('Gênero: ', "tipo", "", "tipo");
        $rel->definirQuebra('Descrição: ', "descricao", "tipo", "descricao");
        
        //exemplo de definição de links
        $rel->definirLink('codigo', 'https://www.google.com.br/search?site=&source=hp&q=[descricao]%20[tipo]&oq=[descricao]%20[tipo]', 0);
        $rel->definirLink('descricao', 'https://www.google.com.br/search?site=&source=hp&q=[tipo]&oq=[tipo]', 1);
        
        //resultSet  de exemplo
        $resultSet = array(
            array('codigo'=>'1', 'descricao'=>'Arroz Pitangueira','valor'=>'12.00','peso'=>'5.00','tipo'=>'Alimentício Básico'),
            array('codigo'=>'1', 'descricao'=>'Arroz Pitangueira','valor'=>'12.00','peso'=>'5.00','tipo'=>'Alimentício Básico'),
            array('codigo'=>'1', 'descricao'=>'Arroz Pitangueira','valor'=>'12.00','peso'=>'5.00','tipo'=>'Alimentício Básico'),
            array('codigo'=>'1', 'descricao'=>'Arroz Pitangueira','valor'=>'12.00','peso'=>'5.00','tipo'=>'Alimentício Básico'),
            array('codigo'=>'1', 'descricao'=>'Arroz Pitangueira','valor'=>'12.00','peso'=>'5.00','tipo'=>'Alimentício Básico'),
            array('codigo'=>'2', 'descricao'=>'Arroz Agulhinha','valor'=>'10.60','peso'=>'5.00','tipo'=>'Alimentício Básico'),
            array('codigo'=>'3', 'descricao'=>'Feijão Carunchão','valor'=>'8.30','peso'=>'1.00','tipo'=>'Alimentício Básico'),
            array('codigo'=>'4', 'descricao'=>'Massa de Tomate Elefante','valor'=>'1.85','peso'=>'0.150','tipo'=>'Tempero'),
            array('codigo'=>'4', 'descricao'=>'Massa de Tomate Elefante','valor'=>'1.85','peso'=>'0.150','tipo'=>'Tempero'),
            array('codigo'=>'4', 'descricao'=>'Massa de Tomate Elefante','valor'=>'1.85','peso'=>'0.150','tipo'=>'Tempero'),
            array('codigo'=>'5', 'descricao'=>'Danone','valor'=>'2.20','peso'=>'0.050','tipo'=>'Guloseima'),
            array('codigo'=>'5', 'descricao'=>'Danone','valor'=>'2.20','peso'=>'0.050','tipo'=>'Guloseima'),
            array('codigo'=>'5', 'descricao'=>'Danone','valor'=>'2.20','peso'=>'0.050','tipo'=>'Guloseima'),
            array('codigo'=>'5', 'descricao'=>'Danone','valor'=>'2.20','peso'=>'0.050','tipo'=>'Guloseima'),
            array('codigo'=>'5', 'descricao'=>'Danone','valor'=>'2.20','peso'=>'0.050','tipo'=>'Guloseima'),
            array('codigo'=>'5', 'descricao'=>'Danone','valor'=>'2.20','peso'=>'0.050','tipo'=>'Guloseima'),
            array('codigo'=>'6', 'descricao'=>'Desodorante Spray','valor'=>'6.00','peso'=>'0.300','tipo'=>'Higiene e Limpeza'),
        );
        
//        //descomente este trecho de código para verificar como é a estrutura de $resultSet
//        echo "<pre>";
//        var_dump($resultSet);
//        echo "</pre>";
//        exit;
        
        //envio dos dados para a view
        return new ViewModel(array('relatorio'=>$rel, 'resultSet'=>$resultSet));
    }
}
